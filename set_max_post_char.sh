#!/bin/bash

# Set the path to your mastodon installation
# absolute or relative to the script path
# Don't forget the tailing slash
MASTO_PATH='live/'

if [[ $1 -eq 0 ]]
then
    echo 'You need to provide the max character length'
    exit 1
else
    NEW_CHAR=$1
fi

if [[ $2 -eq 0 ]]
 then
    echo 'No old value has been provide so the default of 500 will be used'
    OLD_CHAR=500
else
    OLD_CHAR=$2
fi

echo "The max character count will be set to $NEW_CHAR from $OLD_CHAR"

sed -i "s/\<MAX_CHARS = $OLD_CHAR\>/MAX_CHARS = $NEW_CHAR/g" "${MASTO_PATH}app/validators/status_length_validator.rb"
sed -i "s/\<length(fulltext) > $OLD_CHAR\>/length(fulltext) > $NEW_CHAR/g" "${MASTO_PATH}app/javascript/mastodon/features/compose/components/compose_form.jsx"
sed -i "s/\<CharacterCounter max={$OLD_CHAR}\>/CharacterCounter max={$NEW_CHAR}/g" "${MASTO_PATH}app/javascript/mastodon/features/compose/components/compose_form.jsx"

echo 'done!'
