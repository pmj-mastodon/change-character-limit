# Change character limit

This is a small script to change the character limit of a Mastodon instance in one go.

## How to use it

* Copy the set_max_post_char.sh somewhere useful and make it executable
* Set the MASTO_PATH
* Run ```set_max_post_char.sh new_char_length (old_char_length)```  
  The old_char_length is optional and only useful if you already set a charlimit and want to change it again
* Then you need to compile the assets with ```RAILS_ENV=production bundle exec rails assets:precompile```
* Restart mastodon-web ```service mastodon-web restart```
* Done!


## Note

After every update of Mastodon the max char will be reset to the default 500 character, so you need to change it after every update!
